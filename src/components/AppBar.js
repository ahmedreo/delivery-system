import React from 'react';
import { withStyles , AppBar , Toolbar , Typography } from '@material-ui/core';
import Container from './grid/Container'

const styles = theme => ({
  clearPadding: {
    padding: 0
  }
})

function ButtonAppBar(props) {
  const { classes } = props;
  return (
    <AppBar position="static">
      <Container>
        <Toolbar disableGutters>
          <Typography variant="title" color="inherit">
            نظام التسليم
          </Typography>
        </Toolbar>
      </Container>
    </AppBar>
  );
}

export default withStyles(styles)(ButtonAppBar);