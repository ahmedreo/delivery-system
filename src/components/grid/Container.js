import React, { Component } from 'react';
import { withStyles , Grid } from '@material-ui/core'

const styles = theme => ({
	root: {
		width: `100%`,
		padding: `0 15px`,
		margin: `0 auto`,
		[theme.breakpoints.up('sm')]: {
			maxWidth: `600px`,
		},
		[theme.breakpoints.up('md')]: {
			maxWidth: `960px`,
		},
		[theme.breakpoints.up('lg')]: {
			maxWidth: `1280px`,
		}
	}
})

class Container extends Component {
	render() {
		const { classes } = this.props 
		return (
			<Grid container className={classes.root}>
				{this.props.children}
			</Grid>
		)
	}
}

export default withStyles(styles)(Container)