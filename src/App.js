import React, { Component } from 'react';
import { Switch , Route } from 'react-router-dom'
import { } from '@material-ui/core'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import RTL from './rtl'
import './App.css'
import AppBar from './components/AppBar'
import Home from './views/Home'
import Reception from './views/Reception'

const theme = createMuiTheme({
  direction: 'rtl',
  typography : {
    fontFamily: [
      'Cairo'
    ]
  }
});

class App extends Component {
  render() {
    return (
      <RTL>
        <MuiThemeProvider theme={theme}>
          <AppBar/>
          <Switch>
            <Route exact path='/' component={Home}/>
            <Route path='/reception' component={Reception}/>
          </Switch>
        </MuiThemeProvider>
      </RTL>
    );
  }
}

export default App;
