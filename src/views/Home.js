import React, { Component } from 'react';
import { Grid , Typography , withStyles } from '@material-ui/core'
import Card from '../components/Card'
import Container from '../components/grid/Container'

const styles = {
	space: {
		marginTop: 30,
		marginBottom: 30,
	} 
}
	
class Home extends Component {
	render() {
		const classes  = this.props.classes
		return (
			<Container>
				<Grid container>
					<Typography></Typography>
				</Grid>
				<Grid container spacing={16}>
					<Grid item xs={12} sm={4} align="center" className={classes.space}>
						<Card title="الاستقبال" img="/img/reception.jpg"></Card>
					</Grid>
					<Grid item xs={12} sm={4} align="center" className={classes.space}>
						<Card title="الاستعلام" img="/img/inquery.jpg"></Card>
					</Grid>
					<Grid item xs={12} sm={4} align="center" className={classes.space}>
						<Card title="الاستلام" img="/img/receiving.jpg"></Card>
					</Grid>
					<Grid item xs={12} sm={4} align="center" className={classes.space}>
						<Card title="الاستخراج" img="/img/extraction.gif"></Card>
					</Grid>
					<Grid item xs={12} sm={4} align="center" className={classes.space}>
						<Card title="التعبئة" img="/img/packing.jpg"></Card>
					</Grid>
					<Grid item xs={12} sm={4} align="center" className={classes.space}>
						<Card title="الاحصائيات" img="/img/statistics.jpg"></Card>
					</Grid>
				</Grid>
			</Container>
		);
	}
}

export default withStyles(styles)(Home)
